<?php
  class Elang extends Hewan
  {
    public function __construct($nama = "nama", $jumlahKaki = 0, $keahlian = "keahlian",$attackPower="attackPower", $defensePower="defensePower") {
        parent::__construct($nama, $jumlahKaki = 2, $keahlian = "terbang tinggi", $attackPower = 10, $defensePower = 5);
        // $this->nama = __CLASS__;
    }

    public function getInfoHewan(){
      echo "<hr><b>Deskripsi Elang</b><br>";
      echo "Nama Hewan \t = {$this->nama}<br>";
      echo "Jenis Hewan \t = ".__CLASS__."<br>";
      echo "Sisa darah \t = {$this->darah}<br>";
      echo "Jumlah kaki \t = {$this->jumlahKaki}<br>";
      echo "keahlian \t = {$this->keahlian}<br>";
      echo "Attack power \t = {$this->attackPower}<br>";
      echo "Defense power \t = {$this->defensePower}<br>"; 
    }

  }