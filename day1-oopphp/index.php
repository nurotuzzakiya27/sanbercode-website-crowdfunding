<?php
  require_once 'init.php';

  /*-----------------------------*/
  /*-------- Instansiasi --------*/
  /*-----------------------------*/
  $elang = new Elang("elang_1");
  $harimau = new Harimau("harimau_2");

  echo "<hr><b>Instansiasi Elang</b>";
  echo '<pre>';var_dump($elang);echo '</pre>';
  echo "<hr><b>Instansiasi Harimau</b>";
  echo '<pre>';var_dump($harimau);echo '<pre>';

  /*-------------------------*/
  /*-------- Atraksi --------*/
  /*-------------------------*/
  echo "<hr><b>Atraksi</b><br>";
  echo $elang->atraksi();
  echo $harimau->atraksi();

  /*------------------------*/
  /*-------- Battle --------*/
  /*------------------------*/
  echo "<hr><b>Pertandingan Elang VS Harimau</b><br>";

  echo "<u>Ronde 1</u><br>";
  echo "Aksi => ".$elang->serang($harimau)." dan ".$harimau->diserang($elang)."<br>";
  echo $elang->getSisaDarah();
  echo $harimau->getSisaDarah();
  
  echo "<u>Ronde 2</u><br>";
  echo "Aksi => ".$harimau->serang($elang)." dan ".$elang->diserang($harimau)."<br>";
  echo $elang->getSisaDarah();
  echo $harimau->getSisaDarah();
  
  echo "<u>Ronde 3</u><br>";
  echo "Aksi => ".$elang->serang($harimau)." dan ".$harimau->diserang($elang)."<br>";
  echo $elang->getSisaDarah();
  echo $harimau->getSisaDarah();
  
  echo "<u>Ronde 4</u><br>";
  echo "Aksi => ".$harimau->serang($elang)." dan ".$elang->diserang($harimau)."<br>";
  echo $elang->getSisaDarah();
  echo $harimau->getSisaDarah();
  
  echo "<u>Ronde 5</u><br>";
  echo "Aksi => ".$harimau->serang($elang)." dan ".$elang->diserang($harimau)."<br>";
  echo $elang->getSisaDarah();
  echo $harimau->getSisaDarah();
  
  /*---------------------------------*/
  /*-------- Informasi Hewan --------*/
  /*---------------------------------*/
  echo $elang->getInfoHewan();
  echo $harimau->getInfoHewan();

?>