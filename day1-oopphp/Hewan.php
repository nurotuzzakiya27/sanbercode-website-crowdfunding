<?php
	abstract class Hewan {
    use Fight;
    protected $nama, 
           $darah = 50,
           $jumlahKaki,
           $keahlian;

    public function __construct($nama="nama",$jumlahKaki=0, $keahlian="keahlian", $attackPower="attackPower", $defensePower="defensePower") {
        $this->nama = $nama;
        $this->jumlahKaki = $jumlahKaki;
        $this->keahlian = $keahlian;
        $this->attackPower = $attackPower;
        $this->defensePower = $defensePower; 
    }

    public function getSisaDarah(){
      return "Sisa darah {$this->nama} adalah {$this->darah}<br>";
    }
    public function atraksi(){
      return "{$this->nama} sedang {$this->keahlian}<br>";
    }

    abstract public function getInfoHewan();

  }