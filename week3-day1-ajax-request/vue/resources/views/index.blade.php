<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <div id="app">
        <form>
            <input type="text" v-model="selectedUserName">
            <button @click.prevent="addUser(selectedUserName)" v-show="!updateSubmit" >Add</button>
            <button @click.prevent="updateUser()" v-show="updateSubmit">Update</button>
        </form>
        <div v-for="(user, index) in users">
            <ul>
                <li>
                    @{{ user.name }}
                    <button @click="editUser(user, index)"> Edit </button> 
                    || 
                    <button @click="deleteUser(user, index)"> Delete </button>
                </li>
            </ul>
        </div>
        <br>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue-resource@1.5.1"></script>
    <script>
        var data = {
            selectedUserId:null,
            selectedUserName:"",
            updateSubmit:false,
            users: []
        }
        
        var vo = new Vue({
            el: '#app',
            data: data,
            methods: {
                addUser:function(selectedUserName){
                    if(this.selectedUserName != ''){
                        this.$http.post('/api/user', {name: selectedUserName}).then(response => {
                            this.users.unshift({name:selectedUserName})
                            this.selectedUserName = ""
                        });
                    }
                },
                editUser:function(user, index){
                    this.selectedUserName = user.name
                    this.updateSubmit = true
                    this.selectedUserId = index
                },
                updateUser:function(){
                    this.$http.post('/api/user/edit', {id: this.users[this.selectedUserId].id, name: this.selectedUserName }).then(response => {
                        this.users[this.selectedUserId].name = this.selectedUserName 
                        this.selectedUserName = ""
                        this.updateSubmit = false
                        this.selectedUserId = null
                    }); 
                },
                deleteUser: function(user, index){
                    var rmUser = confirm("Anda Yakin akan menghapus nama "+this.users[index].name+" ?");
                    if (rmUser == true) {
                        this.$http.post('/api/user/delete/'+user.id).then(response => {
                            this.users.splice(index, 1);
                        }); 
                    }
                }
            },
            mounted: function(){
              // GET /someUrl
              this.$http.get('/api/user').then(response => {
                let result = response.body.data
                this.users = result
              });
            }
        })
    </script>
</body>
</html>