<p align="center"><a href="#" target="_blank"><img src="https://gitlab.com/nurotuzzakiya27/sanbercode-website-crowdfunding/-/raw/master/website-crowdfunding/public/demo-app/sanbercode.png" width="400"></a></p>


## Screenshot Aplikasi

- Home: <a href="https://gitlab.com/nurotuzzakiya27/sanbercode-website-crowdfunding/-/blob/master/website-crowdfunding/public/demo-app/Home.PNG">link</a>
- Login success: <a href="https://gitlab.com/nurotuzzakiya27/sanbercode-website-crowdfunding/-/blob/master/website-crowdfunding/public/demo-app/Login%20success.PNG">link</a>
- Email and password required: <a href="https://gitlab.com/nurotuzzakiya27/sanbercode-website-crowdfunding/-/blob/master/website-crowdfunding/public/demo-app/Email%20salah.PNG">link</a>
- Logout success: <a href="https://gitlab.com/nurotuzzakiya27/sanbercode-website-crowdfunding/-/blob/master/website-crowdfunding/public/demo-app/logout%20success.PNG">link</a>
- Campaign: <a href="https://gitlab.com/nurotuzzakiya27/sanbercode-website-crowdfunding/-/blob/master/website-crowdfunding/public/demo-app/campaign.PNG">link</a>
- All Campaign: <a href="https://gitlab.com/nurotuzzakiya27/sanbercode-website-crowdfunding/-/blob/master/website-crowdfunding/public/demo-app/all%20campaing.PNG">link</a>
- Donate success: <a href="https://gitlab.com/nurotuzzakiya27/sanbercode-website-crowdfunding/-/blob/master/website-crowdfunding/public/demo-app/Donate%20success.PNG">link</a>
- Search: <a href="https://gitlab.com/nurotuzzakiya27/sanbercode-website-crowdfunding/-/blob/master/website-crowdfunding/public/demo-app/search.PNG">link</a>
- Search not found: <a href="https://gitlab.com/nurotuzzakiya27/sanbercode-website-crowdfunding/-/blob/master/website-crowdfunding/public/demo-app/search%20not%20found.PNG">link</a>
- Login success: <a href="https://gitlab.com/nurotuzzakiya27/sanbercode-website-crowdfunding/-/blob/master/website-crowdfunding/public/demo-app/login%20google.PNG">link</a>

## Video Demo

Link Video dapat dilihat pada link berikut: <a href="https://gitlab.com/nurotuzzakiya27/sanbercode-website-crowdfunding/-/blob/master/website-crowdfunding/public/demo-app/demo_aplikasi_crowdfunding.mp4">link</a>

