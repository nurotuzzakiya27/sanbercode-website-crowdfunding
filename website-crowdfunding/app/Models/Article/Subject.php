<?php

namespace App\Models\Article;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    protected $table = 'subjects';
    protected $primaryKey = 'id';
    protected $keyType = 'string';

    protected $fillable = [
        'name', 'slug'
    ];
    public function articles(){
        return $this->hasMany(Article::class);
    }
}
