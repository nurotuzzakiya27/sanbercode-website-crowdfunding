<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\UsesUuid;

class Blog extends Model
{
    Use UsesUuid;

    protected $guarded = [];
}
