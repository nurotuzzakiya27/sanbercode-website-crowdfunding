<?php

namespace App\Models;

use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Str;
use App\Traits\UsesUuid;
use App\Models\Article\Article;
use App\Models\Otp;
use Carbon\Carbon;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable, UsesUuid;

    
    // protected $primaryKey = 'id';
    // protected $keyType = 'string';
    // protected $table ='users';
    public $incrementing = false;

    protected function get_user_role_id(){
        $role = \App\Role::where('name', 'user')->first();
        return $role === null ? null : $role->id;
    }

    public static function boot(){
        parent::boot();

        static::creating(function ($model){
            $model->role_id = $model->get_user_role_id();
            $model->created_at = Carbon::now();
            $model->updated_at = Carbon::now();
        });

        static::created(function($model){
            $model->generate_otp_code();
        });
    } 

    public function isAdmin(){
        if($this->role_id === $this->get_user_role_id()){
            return false;
        }
        return true;
    }
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'username',
        'email',
        'password',
        'role_id',
        'id',
        'photo'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function role()
    {
        return $this->belongsTo(Role::class, "role_id", "id");
    }

    public function otp()
    {
        return $this->hasOne(Otp::class, "user_id");
    }

    // Rest omitted for brevity

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function articles(){
        return $this->hasMany(Article::class);
    }

    public function generate_otp_code(){
        do{
            $random =  rand(100000,999999);
            $check = Otp::where('otp', $random)->first();
        }while($check);

        $now = Carbon::now();
        // dd($this->id);
        $otp_code = Otp::updateOrCreate(
            ['user_id' => $this->id,
            'otp'     => $random, 
            'expire_date' => Carbon::now()->addMinutes(5)]
        );
    }

}
