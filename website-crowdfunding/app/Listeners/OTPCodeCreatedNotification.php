<?php

namespace App\Listeners;

use App\Events\OTPCodeCreated;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Mail;
use App\Models\User;
use App\Mail\OtpCodeCreatedMail;

class OTPCodeCreatedNotification implements ShouldQueue
{
    public $user;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Handle the event.
     *
     * @param  OTPCodeCreated  $event
     * @return void
     */
        public function handle(OTPCodeCreated $event)
        {
            Mail::to($event->user->email)->send(new OtpCodeCreatedMail($event->user));
        }
}
