<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class RegisterResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $arrData = [
            'response_code' => '00',
            'response_message' => 'Silahkan cek email',
            'data' => [
                'user'=> [
                    'name'     => $this->name,
                    'email' => $this->email,
                    'updated_at' => $this->updated_at->format('Y-m-d H:i:s'),
                    'created_at'   => $this->created_at->format('Y-m-d H:i:s'),
                    'id'    => $this->id
                ]
            ]
            
        ];
        return $arrData;
    }
}
