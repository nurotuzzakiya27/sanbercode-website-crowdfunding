<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UpdatePasswordController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $request->validate([
            'email' => 'required|exists:users',
            'password' => 'required|confirmed|min:6'
        ]);

        $user = User::where('email', $request->email)
                ->update(['password' => Hash::make($request->password)]);
        // $user = User::where('email', $request->email)->first();
        // $user->password = Hash::make($request->password);
        // $user->save();

        return response()->json([
            'response_code'    => "00",
            'response_message' => 'Password berhasil diubah!'
        ], 200);
    }
}
