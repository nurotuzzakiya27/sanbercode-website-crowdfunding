<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Otp;
use App\Models\User;
use Carbon\Carbon;

class VerificationController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $request->validate([
            'otp' => 'required'
        ]);

        $otp = Otp::where('otp', $request->otp)->first();

        //cek otp is existing
        if (!$otp) {
            return response()->json([
                'response_code'    => '01',
                'response_message' => 'Kode OTP tidak ditemukan!'
            ], 200);
        }

        //cek tanggal expired date
        $now = Carbon::now();
        if ($now > $otp->expire_date) {
            return response()->json([
                'response_code'    => '01',
                'response_message' => 'Kode OTP sudah tidak berlaku, silahkan generate ulang!'
            ]);
        }

        //update data user
        $user = User::find($otp->user_id);
        $user->email_verified_at = Carbon::now();
        $user->save();

        //delete otp
        $otp->delete();

        $data['user'] = $user;

        // $otp->user->email_verified_at = Carbon::now();
        // $otp->push();
        // $user = $otp->user;
        // $otp->delete();

        return [
            'response_code'    => '00',
            'response_message' => 'Akun berhasil terverifikasi!',
            'data'             => $user
        ];
    }
}
