<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Events\OTPCodeCreated;

class RegenerateOtpCodeController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $request->validate([
            'email' => 'required|exists:users'
        ]);

        $user = User::where('email', $request->email)->first();
        $user->generate_otp_code();

        $data['user'] = $user;

        // $newOtp = new OtpCodeController();
        // $newOtp = $newOtp->generator($user);
        // unset($newOtp['user']);
        // unset($user['otp']);
        // $user['otp'] = $newOtp;

        event(new OTPCodeCreated($user));
        
        return [
            'response_code'    => "00",
            'response_message' => 'silahkan cek email',
            'data'             => $data
        ];


    }
}
