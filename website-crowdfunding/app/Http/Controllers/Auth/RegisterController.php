<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Auth\RegisterRequest;
use App\Http\Resources\RegisterResource;
use App\Models\User;
use App\Models\Role;
use App\Events\UserRegisteredEvent;
use App\Events\OTPCodeCreated;

class RegisterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request, User $user)
    {
        $request->validate([
            'name'  => 'required',
            'email' => 'required|unique:users'
            ]);

            $data_request = $request->all();
        $user = User::create($data_request);

        $data['user'] = $user;

        event(new UserRegisteredEvent($user));
        event(new OTPCodeCreated($user));

        // $otp = new OtpCodeController();
        // $user = new User([
        //     'name'  => $request->name,
        //     'email' => $request->email
        // ]);
        // $user->role()->associate(Role::find('b35d4e28-194c-40da-8a9d-2f5af9f89731'));
        // $user->save();

        // $otp->generator($user);

        return [
            'response_code'    => "00",
            'response_message' => 'User baru berhasil ditambahkan, silahkan cek email untuk melihat kode otp',
            'data'             => $user
        ];

    }
}
