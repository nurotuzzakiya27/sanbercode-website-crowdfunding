<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;

class ProfileController extends Controller
{
    public function show(){
        $data['user'] = auth()->user();
        return response()->json([
            'response_code'    => "00",
            'response_message' => 'Profile berhasil ditampilkan',
            'data'             => $data
        ]);
    }

    public function update(Request $request){
        $user = auth()->user();

        if($request->hasFile('photo')){

            $photo_profile = $request->file('photo');
            $photo_profile_extension = $photo_profile->getClientOriginalExtension();
            $photo_profile_name = Str::slug($user->name, '-') .  '-' . $user->id . '-' . $photo_profile_extension;
            $photo_profile_folder = 'photo/users/photo-profile/';
            $photo_profile_location = $photo_profile_folder . $photo_profile_name;

            try {
                //code...
                $photo_profile->move(public_path($photo_profile_location), $photo_profile_name);
                $user->update([
                    'photo' => $photo_profile_location
                ]);
            } catch (\Throwable $th) {
                //throw $th;
                return response()->json([
                    'response_code'    => "01",
                    'response_message' => 'Photo Profile gagal upload'
            ], 200);
            }
            // dd($photo_profile_location);
        }
            
        $user->update([
            'name' => $request->name,
        ]);
        $data['user'] = $user;
        // $request->validate([
        //     "name"  => "nullable",
        //     "photo" => "nullable|mimes:jpeg,bmp,png,jpg"
        // ]);
        
        // $user = $request->user();
        // $user->name = $request->name ?: $user->name;
        
        // if (null !== $image = $request->photo) {
        //     $tmpFilePath = $this->getTempPath($image->getClientOriginalExtension());
        //     $upload = $this->image->make($image->getRealPath())->save($tmpFilePath);
        
        //     $filePath = sprintf('profiles/%s/', $user->id) . uniqid() . '.' . $image->getClientOriginalExtension();
        //     Storage::put('public/'.$filePath, file_get_contents($tmpFilePath));
        //     $user->photo = $filePath;
        
        //     $upload->destroy();
        // }
        
        // $user->save();
            
        return response()->json([
                'response_code'    => "00",
                'response_message' => 'Profile berhasil diupdate',
            'data'             => $user
        ], 200);
    }

}
