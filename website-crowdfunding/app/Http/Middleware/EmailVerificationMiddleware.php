<?php

namespace App\Http\Middleware;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use Closure;

class EmailVerificationMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // dd(Auth::user());
        $isVerifiedEmail = Auth::user()->email_verified_at;
        $isVerifiedPassword = Auth::user()->password;
        if (!is_null($isVerifiedEmail) && !is_null($isVerifiedPassword)) {
            return $next($request);
        }

        // return redirect()->back();
        return response()->json([
            'message' => 'Email anda belum terverifikasi'
        ]);

    }
}
