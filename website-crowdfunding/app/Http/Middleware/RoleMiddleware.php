<?php

namespace App\Http\Middleware;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use Closure;

class RoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!is_null(Auth::user()->role_id)){
            $name = Auth::user()->role->name;
            if (is_array($roles)) {
                foreach ($roles as $key => $role) {
                    if ($name === $role) {
                        return $next($request);
                    }
                }
            }
            if ($roles === $name) {
                return $next($request);
            }
        }

        return redirect()->back();

    }
}
