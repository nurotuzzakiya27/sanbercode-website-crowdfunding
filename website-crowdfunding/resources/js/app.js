import Vue from 'vue'
import router from './router.js'
import App from './App.vue'
import store from './store.js'
import vuetify from './plugins/vuetify.js'
import Vuex    from 'vuex'
import './bootstrap.js'

Vue.use(Vuex)
// const store = new Vuex.Store({
//     state: {
//         count: 0
//     },
//     mutations: {
//         increment (state) {
//             state.count++
//         }
//     }
// })


const app = new Vue({
    el: '#app',
    store,
    router,
    vuetify,
    components:{
        App
    },
})