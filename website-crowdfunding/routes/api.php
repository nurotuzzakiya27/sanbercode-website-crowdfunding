<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'middleware' => 'api',
    'prefix'     => 'campaign'
], function () {
    Route::get('random/{count}', 'CampaignController@random');
    Route::post('store', 'CampaignController@store');
    Route::get('/', 'CampaignController@index');
    Route::get('{id}', 'CampaignController@detail');
    Route::get('/search/{keyword}', 'CampaignController@search');
});

Route::group([
    'middleware' => 'api',
    'prefix'     => 'blog'
], function () {
    Route::get('random/{count}', 'BlogController@random');
    Route::post('store', 'BlogController@store');

});

/* ------------------------------------------------------------------- */
/* Tugas day 4&5 - Membuat REST API dan Authentication menggunakan JWT */
/* ------------------------------------------------------------------- */
Route::group([
    'middleware'    => 'api',
    'prefix'        => 'auth',
    'namespace'     => 'Auth'
], function(){
    Route::post('register', 'RegisterController');
    Route::post('verification', 'VerificationController');
    Route::post('regenerate-otp', 'RegenerateOtpCodeController');
    Route::post('update-password', 'UpdatePasswordController');
    Route::post('login', 'LoginController');
    Route::post('logout', 'LogoutController')->middleware('auth:api');
    Route::post('check-token', 'CheckTokenController')->middleware('auth:api');

    Route::get('/social/{provider}', 'SocialiteController@redirectToProvider');
    Route::get('/social/{provider}/callback', 'SocialiteController@handleProviderCallback');
}); 

Route::group([
    'middleware'    => ['api','emailVerify', 'auth:api'],
    'prefix'        => 'profile'
], function(){
    Route::get('get-profile', 'ProfileController@show');
    Route::post('update-profile', 'ProfileController@update');
}); 


/* -------------------------------------------------------------------- */
/* Materi day 4&5 - Membuat REST API dan Authentication menggunakan JWT */
/* -------------------------------------------------------------------- */

// Route::namespace('Auth')->group(function(){
//     Route::post('register', 'RegisterController');
//     Route::post('login', 'LoginController');
//     Route::post('logout', 'LogoutController');

// });

// Route::namespace('Article')->middleware('auth:api')->prefix('article')->group(function(){
//     Route::post('create', 'ArticleController@store');
//     Route::get('{article}', 'ArticleController@show');
//     Route::get('/', 'ArticleController@index');
//     Route::patch('{article}', 'ArticleController@update');
//     Route::delete('{article}', 'ArticleController@destroy');

// });


// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

// Route::get('user', 'UserController');
