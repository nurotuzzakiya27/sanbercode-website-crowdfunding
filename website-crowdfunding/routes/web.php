<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::view('/{any?}', 'app')->where('any', '.*');
// Route::get('/{any?}', function () {
//     return 'masuk';
// })->where('any', '.*');
// Route::get('/app', function () {
//     return view('app');
// });

// Auth::routes();
// Route::get('/', function () {
//     return view('welcome');
// });

// Route::group(['middleware' => ['auth','emailVerify']], function () {
//     Route::get('/route-1', 'UserController@route1');

//     Route::group(['middleware' => 'role:admin'], function () {
//         Route::get('/route-2', 'UserController@route2');
//     });

// });

// Route::get('/masuk', 'UserController@masuk');//->middleware('role');
// Route::get('/home', 'HomeController@index')->name('home');

