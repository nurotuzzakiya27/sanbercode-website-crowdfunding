<?php

use Illuminate\Database\Seeder;
use App\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $listRole = [
            [
                'id'    =>  Str::uuid(),
                'name'  => 'admin'
            ],
            [
                'id'    =>  Str::uuid(),
                'name'  => 'user'
            ]
        ];
        Role::insert($listRole);
        
        // $user = [
        //     'admin',
        //     'user'
        // ];

        // $id = [
        //     "ab1ec334-a8cb-4928-bc42-9551d315ca88",
        //     "b35d4e28-194c-40da-8a9d-2f5af9f89731"

        // ];

        // $count = 0;
        // foreach ($user as $role) {
        //     Role::insert([
        //         'id' => $id[$count],
        //         'name' => $role
        //     ]);
        //     $count++;
        // }

    }
}
