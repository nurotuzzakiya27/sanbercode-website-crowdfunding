<?php

use Illuminate\Database\Seeder;
use App\Models\Article\Subject;

class SubjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $listSubject = [
            [
                'name'  => 'laravel',
                'slug'  => 'laravel'
            ],
            [
                'name'  => 'php',
                'slug'  => 'php'
            ]
        ];
        Subject::insert($listSubject);
    }
}
