<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $name = [
            'admin',
            'user'
        ];
        $email = [
            'admin@admin.admin',
            'user@user.user'
        ];
        $pw = Hash::make(12345678);
        $role = [
            'ab1ec334-a8cb-4928-bc42-9551d315ca88',
            ''
        ];
        $count = 0;
        foreach ($name as $nm) {
            User::create([
                'name' => $nm,
                'email' => $email[$count],
                'password' => Hash::make(12345678),
                'role_id' => $role[$count]
            ]);
            $count++;
        }
    }
}
